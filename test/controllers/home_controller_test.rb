require 'test_helper'

class HomeControllerTest < ActionDispatch::IntegrationTest
  test '#show' do
    get root_url, params: { name: "Adastra", gender: "He" }
    assert_response :success

    assert_includes response.body, "Adastra"
  end
end
