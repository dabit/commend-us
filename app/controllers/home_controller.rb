class HomeController < ApplicationController
  def show
    @name = params[:name]
    @gender = params[:gender]

    if @name.present? && @gender.present?
      @commendation = I18n.t(:phrase, name: @name, gender: @gender).sample(5).join(". ")
    end
  end
end
